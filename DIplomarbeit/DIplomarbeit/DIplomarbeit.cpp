// DIplomarbeit.cpp: Hauptprojektdatei.

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

int backpropagation(int,float);
int differenz();
int gewicht();
using namespace System;
float weight[10][10];

int gewicht(){
	for (int i = 0; i < 10; i++){
		for (int j = 0; j < 10; j++){
			weight[i][j] = ((float)rand()) / (float)RAND_MAX;
			printf("Das normale Gewicht ist %f\n", weight[i][j]);
		}

	}
	differenz();
	return 0;
}

int differenz(){
	float neuron_soll[10], neuron_ist[10], fehler[10];
	int neuron_nr;



	for (int i = 0; i < 10; i++){
		neuron_soll[i] = ((float) rand())/(float)RAND_MAX; //Zufallszahlen f�r Differenz generieren
		neuron_ist[i] = ((float)rand()) / (float)RAND_MAX; //Zufallszahlen f�r Differenz generieren
		//printf("Das ist der %d.te Wert: %f\n", i + 1, neuron_soll[i]); //Ausgabe zur Kontrolle
		}

		float test;

	for (int i = 0; i < 10; i++){
		
		test = fabs(neuron_ist[i] - neuron_soll[i]);
		printf("Der Fehler ist %f\n", test);
		if (test > 0.4)
		{
			neuron_nr = i;
			printf("\nDas Neuron ist %d \n", i);
			backpropagation(neuron_nr, test);
		}

	}
	for (int i = 0; i < 10; i++){
		printf("Das Gewicht ist %f\n", weight[i][neuron_nr]);
	}
	return 0;
	
}
int backpropagation(int neuron_nr, float test{
	float lernrate = 0.01;
	

	for (int i = 0; i < 10; i++){
		weight[neuron_nr][i] = test*lernrate;
	}
	return 0;
}

int main(){
	gewicht();
	getchar();
	return 0;
}