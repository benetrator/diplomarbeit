\contentsline {section}{\numberline {1}Aufgabenstellung}{2}{section.1}
\contentsline {section}{\numberline {2}Problem-Analyse}{3}{section.2}
\contentsline {subsubsection}{\numberline {2.0.1}Methodenplan}{3}{subsubsection.2.0.1}
\contentsline {subsection}{\numberline {2.1}L\IeC {\"o}sungswege}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}ANN-Lernverfahren}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Programmierung}{5}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Nutzwertanalysen}{5}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Entscheidungsvarianten}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Definition von Bewertungskriterien}{5}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Gewichtung der Bewertungskriterien}{5}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Festlegung des Bewertungsma\IeC {\ss }stabes}{5}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Punktevergabe}{6}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Summierung der Auswahl}{8}{subsubsection.2.2.6}
\contentsline {section}{\numberline {3}Testplan}{9}{section.3}
\contentsline {section}{\numberline {4}Grobdesign}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Trainingsteil}{10}{subsection.4.1}
\contentsline {section}{\numberline {5}Aufwand-Sch\IeC {\"a}tzung}{10}{section.5}
\contentsline {subsection}{\numberline {5.1}Projektplanung}{10}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Programmierung}{10}{subsection.5.2}
\contentsline {section}{\numberline {6}Zeitplan}{11}{section.6}
\contentsline {section}{\numberline {7}Kostenkalkulation, Kostenvorschlag}{11}{section.7}
\contentsline {section}{\numberline {8}Schritt f\IeC {\"u}r Schritt zum neuronalen Netz}{11}{section.8}
